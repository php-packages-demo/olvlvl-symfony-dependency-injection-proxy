# [olvlvl](https://phppackages.org/s/olvlvl)/[symfony-dependency-injection-proxy](https://phppackages.org/p/olvlvl/symfony-dependency-injection-proxy)

Generate super tiny proxies for Symfony's dependency injection

Unofficial howto and demo

## Documentation
* [*I wrote a proxy generator for Symfony's dependency injection*](https://olvlvl.com/2018-10-dependency-injection-proxy)
  2018 [Olivier Laviale](https://olvlvl.com/)